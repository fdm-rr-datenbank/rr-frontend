# Beschreibung des Repositorien-Recommenders
Der Repositorien-Recommender dient der Auswahl und Anzeige fachbezogener Repositorien, wobei die Basis eine bereits bestehende Excel-Datei bildet.
Die in der Excel-Datei hinterlegten Daten werden durch Konkatenation in ein SQL-Statement umgewandelt, welche anschließend mit einem Datenbankmanagementsystem auf eine MariaDB übertragen werden. Das Frontend greift via PDO auf die Datenbank zu und ruft die gegebenenfalls gefilterten Daten ab, um sie anschließend zu visualisieren.

# Funktionalität
- Zwei Filterebenen:
- - Filtern nach Nachnutzen, Archivieren, Veröffentlichen, Sonstiges
- - Anschließende (optionale) Filterung nach Fachbereich
- Anzeigen weiterer Informationen durch Button
- Listung der Seite nach Priorität (gemäß den Angaben in der Excel-Datei)

# noch zu implementierende Funktionalität
- Navigationsbuttons (Anzeigen von ca. 10 Ergebnissen, weitere Ergebnisse mit vor- und zurück) 
- - Gegenwärtig ist in dem Code die Funktion für Ergebnis-Navigationsbuttons (z.B. Zeige nur 10 Ergebnisse und vor/zurück-Buttons) ausgeblendet und zugehörige PHP-Elemente sind mit „_deprecated“ gekennzeichnet. Obwohl die Buttons funktional sind, ist es nicht gelungen, die Übergabe der Variablen so zu gestalten, dass sie den Fachbereich/Nutzungsart Filter nicht überschreibt.
- Suchfunktion

# PHP-Frontend
- sämtliche Dateien sollten direkt im Hauptverzeichnis liegen, andernfalls muss der Zugang über eine htaccess-Datei konfiguriert werden
- Gemeinsam genutzte Funktionen wurden als in eigene mit „funktion_*.php“ bezeichnete PHP-Dateien ausgelagert, welche mit include() aufgerufen werden können. Auf die Verwendung von function() wurde verzichtet, da der gewonnene Geschwindigkeitsvorteil in diesem Szenario minimal ist und include() später eine leichtere Ajax-Implementation gestattet.
- In info.php erfolgt der Datenbankzugriff über ein prepared statement, da das SQL-Statement eine via GET übertragene beinhaltet, welche ansonsten leicht für SQL-Injections missbraucht werden kann. Sämtliche anderen Abfragen werden serverseitig über vorher festgelegte Parameter ausgeführt, weshalb an dieser Stelle eine einfache SQL-Injection als Angriffsvektor ausgeschlossen werden kann und auf prepared-statements verzichtet werden kann.
- Ein Auslagern der Datenbankabfrage für die Nutzungsarten in eine eigene Funktion oder externe php-Datei führte zu merklichen und nicht nachvollziehbaren Geschwindigkeitsverlusten, weshalb diese Änderung wieder verworfen wurde.

# Seitenstruktur des RR-Tools: <p align="center"><img src="img/frontend_struktur.JPG" /></p>

# Datenbankinput

- Die Datenbasis stellt ein Excel-File dar, welches zu Verfügung gestellt wurde (data/FDM_DataInput.xlsx)
- Die Eingaben wurden normalisiert und syntaktisch angepasst, die erforderlichen Datentypen und Bedingungen wurden im Excel-File hinterlegt
- die "*_Generation"-benannten Worksheets dienen der Generation des SQL-Insert-Statements durch Excel-Konkatenation, wobei jede Datenhaltende Relation über ein eigenes Worksheet verfügt
- - (u.a.) leere Felder führen zu einer fehlerhaften Syntax, welche unter Umständen korrigiert werden muss
- die zur Datenbankerstellung und -befüllung verwendete Syntax kann data/rr_sql_syntax.sql entnommen werden

# Datenbankstruktur: <p align="center"><img src="img/relationen.JPG" /></p>

