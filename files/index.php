<!DOCTYPE html>

<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<html lang="de">
    <head>
        <meta charset="utf-8" />
        <title>FDM-Repositorien Recommender</title>
        <link rel="stylesheet" href="style.css">
        
         <!--temporärer Fix, um Seitenkopf mit richtigem Abstand anzuzeigen -->
        <style>
            body { 
                padding: 5%; 
                margin: 13%;
                margin-top: 3%;
                padding-top: 3%;
            }
        </style>

    </head>
    
        <!-- Header einbinden-->
        <header id="header">
            <img src="logo.png" align="left" alt="Logo">
            Forschungsdatenmanagement
            <div id="groß">Repositorien Recommender</div>
        </header>

        <!-- vertikale Navigationsleiste-->
        <?php
            include('funktion_navigation.php');
        ?>
        
        <!-- Dropdownmenü zur Auswahl der Nutzungsart-->
        <br>
        <h3> Bitte wählen sie aus, in welcher Form Sie mit Daten interagieren möchten</h3>
        <br>
        <div class="dropdown">
            <button class="dropbtn">Auswahl &#9660</button>
            <div class="dropdown-content">
                <a href="nachnutzen.php">Nachnutzen</a>
                <a href="austausch.php">Austausch</a>
                <a href="veroeffentlichen.php">Veröffentlichen</a>
                <a href="archivieren.php">Archivieren</a>
                <a href="sonstiges.php">Sonstiges</a>
            </div>
        </div> 

    </body>
</html>