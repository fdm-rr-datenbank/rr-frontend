<!DOCTYPE html>

<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<html lang="de">
    <head>
        <meta charset="utf-8" />
        <title>FDM-Repositorien Recommender</title>
        <link rel="stylesheet" href="style.css">
        <style>
            body { 
                padding: 5%; 
                margin: 13%;
                margin-top: 3%;
                padding-top: 3%;
            }
        </style>
    </head>

    <body>
        <header id="header">
            <img src="logo.png" align="left"  alt="Logo">  
                Repositorien Recommender
            <div id="groß">Kontakt</div>
        </header>

        <!-- vertikale Navigationsleiste-->
        <?php
            include('funktion_navigation.php');
        ?>

        <br>
        <h2> Haben Sie weitere Fragen oder Anregungen? </h2>  
        <h3> Besuchen Sie unser Website oder senden Sie uns <a class="plainLink"  href="mailto:"> eine Nachricht</a>. </h3>
        <h3> Bitte beachten sie auch unsere <a class="plainLink" href="disclaimer.php"> Datenschutzhinweise</a>. </h3>
        
    </body>
</html>