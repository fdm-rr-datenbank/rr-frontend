<!DOCTYPE html>

<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<html lang="de">
    <head>
        <meta charset="utf-8" />
        <title>DB-Info</title>
        <link rel="stylesheet" href="style.css">
        <style>
            body { 
                padding: 5%; 
                margin: 13%;
                margin-top: 3%;
                padding-top: 3%;
            }
        </style>
    </head>

    <body>

        <?php // Navigationselemente definieren
            $start = 1; 
            if (isset ($_POST["s"])) {$start = $_POST["s"];}
            if (isset ($_POST["richtung"])) {    
                if ($_POST["richtung"] == "zu") {
                    $start = $start - 5; 
                }
                if ($_POST["richtung"] == "vor") {
                    $start = $start + 5;                 
                }
            }
            if ($start <= 0) {$start = 0;
            }
            if ($start > 10) {$start = 10;
            }
        ?>

        <?php //Verbindung zur Datenbank aufbauen und Fehler abfangen und anzeigen
            include('connect.php');
        ?>
        
        <!-- Header-->
        <header id="header">
            <img src="ogo.png" align="left"  alt="Logo">
            Forschungsdatenmanagement
            <div id="groß">Datenbank-Infosystem</div>
        </header>

        <!-- vertikale Navigationsleiste-->
        <?php
            include('navigation.php');
        ?>
        
        <!-- Navigationselemente anzeigen-->
        <br>
        <div id="links">
            <form method="post" action="index.php" accept-charset="UTF-8">
                <input type = "hidden" name = "s" value = "<?php echo $start; ?>">
                <input type = "hidden" name = "richtung" value = "zu" >
                <input type="submit" name="zurück" value="Zurück"> 
            </form>
        </div>
        <div id="rechts">
            <form method="post" action="index.php" accept-charset="UTF-8">
                <input type = "hidden" name = "s" value = "<?php echo $start; ?>">
                <input type = "hidden" name = "richtung" value = "vor">
                <input type="submit" name="weiter" value="Weiter">
            </form>
        </div>
        <br>
        
        <!-- Ausgegebene SQL Einträge als Tabelle anzeigen inkl. Recherche Button-->
        <br>
        <table id="ausgabe">
            <thead>
                <tr>
                    <td class ="bezeichnung">Bezeichnung</td>
                   <!-- <td class ="bezeichnung">Nebentitel</td>-->
                    <td class ="bezeichnung">Inhalt</td>
                    <td class ="bezeichnung"></td>
                </tr>
            </thead>
            <tbody>
            <?php            
                $sql = "SELECT repositorium.repositorium_titel, repositorium.repositorium_url, repositorium.repositorium_schwerpunkt, repositorium.repositorium_id, fachbereich.repositorium_id, fachbereich.fachbereich_name 
                FROM repositorium INNER JOIN fachbereich ON fachbereich.repositorium_id=repositorium.repositorium_id ".$selector." GROUP BY repositorium.repositorium_titel Limit 5 Offset $start";
                foreach ($pdo->query($sql) as $row) :?> 
                        <tr>
                            <td><?=$row['repositorium_titel']?></td>
                            <!--<td><?=$row['repositorium_titelzusatz']?></td>-->
                            <td><?=$row['repositorium_schwerpunkt']?></td>
                            <td class="bfield"><a class="linkbutton" href="<?=$row['repositorium_url']?>">Recherche<br>starten</a></td>
                        </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
       

    </body>
</html>