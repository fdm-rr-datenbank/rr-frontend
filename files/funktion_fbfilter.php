<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<?php 
    // Anzeigen der aktuellen Fachbereichs-Filterung ("filter")
    $filter = "";
    if(isset($_POST['submit'])){
        if($_POST['fachbereich'] == "AND fachbereich.fachbereich_informatik = 1 "){
            $filter = "ausgewählter Fachbereich: Informatik"; 
        } elseif ($_POST['fachbereich'] == "AND fachbereich.fachbereich_mathematik = 1 "){
            $filter = "ausgewählter Fachbereich: Mathematik";
        } elseif ($_POST['fachbereich'] == "AND fachbereich.fachbereich_maschinenbau = 1 "){
            $filter = "ausgewählter Fachbereich: Maschinenbau";
        } elseif ($_POST['fachbereich'] == "AND fachbereich.fachbereich_landwirtumwelt = 1 "){
            $filter = "ausgewählter Fachbereich: Landwirtschaft und Umwelt";
        } elseif ($_POST['fachbereich'] == "AND fachbereich.fachbereich_chemie = 1 "){
            $filter = "ausgewählter Fachbereich: Chemie";    
        } elseif ($_POST['fachbereich'] == "AND fachbereich.fachbereich_geoinformation = 1 "){
            $filter = "ausgewählter Fachbereich: Geoinformation";   
        } elseif ($_POST['fachbereich'] == "AND fachbereich.fachbereich_bwl = 1 "){
            $filter = "ausgewählter Fachbereich: Wirtschaftswissenschaften"; 
        } elseif ($_POST['fachbereich'] == "AND fachbereich.fachbereich_bauingeneurwesen = 1 "){
            $filter = "ausgewählter Fachbereich: Bauingenieurwesen";
        } elseif ($_POST['fachbereich'] == "AND fachbereich.fachbereich_design = 1 "){
            $filter = "ausgewählter Fachbereich: Gestaltung"; 
        } elseif ($_POST['fachbereich'] == "AND fachbereich.fachbereich_elektrotechnik = 1 "){
            $filter = "ausgewählter Fachbereich: Elektrotechnik"; 
        } else {$filter = "";}
    }
?>