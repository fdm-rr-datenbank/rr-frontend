<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->


<!-- Übergabe der ID der angezeigten Datenbank -->
    <?php $infoId = $_GET["id"]; ?>

<!-- Funktion zur Umwandlung von binärwärten in Boolean -->
    <?php
        function booleanUmwandlung($fi) {
            if($fi == 1) {
                return 'Ja';
            } else {
                return 'Nein';
            }
        }
    ?>

<!-- Funktion zur Umwandlung der Zahlen 1-3 in Strings -->
    <?php
        function tripUmwandlung($fk) {
            if($fk == 1) {
                return 'kostenlos';
            } elseif($fk == 2) {
                return 'kostenpflichtig';
            } else {
                return 'keine Angaben verfügbar';
            }
        }
    ?>

<!-- Funktion zur Vermeidung der Ausgabe leerer Felder -->
    <?php
        function leerUmwandlung($fl) {
            if($fl == "") {
                return 'keine Angaben verfügbar';
            } else {
                return $fl;
            }
        }
    ?>

<!DOCTYPE html>
<html lang="de">

    <head>
        <meta charset="utf-8" />
        <title>Repositorien-Recommender</title>
        <link rel="stylesheet" href="style.css">
        <style>
            body { 
                padding: 5%; 
                margin: 13%;
                margin-top: 3%;
                padding-top: 3%;
            }
        </style>
    </head>

    <body>

        <?php //Verbindung zur Datenbank aufbauen und Fehler abfangen und anzeigen
            include('funktion_connect.php');
        ?>
        
        <!-- Header-->
        <header id="header">
            <img src="logo.png" align="left"  alt="Logo">
            Forschungsdatenmanagement
            <div id="groß">Repositorien Recommender</div>
        </header>

        <!-- vertikale Navigationsleiste-->
        <?php
            include('funktion_navigation.php');
        ?>
        
        <!-- Einlesen der ausgewählten Datensätze aus der Datenbank via prepared statements -->
        <?php            
            $sql = $pdo->prepare('SELECT 
                repositorium.repositorium_name, 
                repositorium.repositorium_url, 
                repositorium.repositorium_beschreibung, 
                repositorium.repositorium_id, 
                repositorium.repositorium_embargo,
                repositorium.repositorium_input,
                repositorium.repositorium_inputrules,
                repositorium.repositorium_inredata,
                repositorium.repositorium_kosten,
                repositorium.repositorium_multiplemeta,
                repositorium.repositorium_ontologie,
                repositorium.repositorium_openaccess,
                repositorium.repositorium_preservation,
                repositorium.repositorium_relevanz,
                repositorium.repositorium_relink,
                repositorium.repositorium_reuse,
                repositorium.repositorium_sharing,
                repositorium.repositorium_veroeffentlichungspflicht
            FROM repositorium WHERE repositorium_id = '.$infoId);
            $sql->execute();

        /* Tupel in Variablen überführen */
            $row = $sql->fetch();
            $infoName = $row["repositorium_name"];
            $infoUrl = $row["repositorium_url"];
            $infoBeschreibung = $row["repositorium_beschreibung"];
            $infoEmbargo = $row["repositorium_embargo"];
            $infoInput = $row["repositorium_input"];
            $infoRules = $row["repositorium_inputrules"];
            $infoRedata = $row["repositorium_inredata"];
            $infoKosten = $row["repositorium_kosten"];
            $infoMeta = $row["repositorium_multiplemeta"];
            $infoOntologie = $row["repositorium_ontologie"];
            $infoOpen = $row["repositorium_openaccess"];
            $infoPreserve = $row["repositorium_preservation"];
            $infoRelevanz = $row["repositorium_relevanz"];
            $infoRelink = $row["repositorium_relink"];
            $infoReuse = $row["repositorium_reuse"];
            $infoSharing = $row["repositorium_sharing"];
            $infoVero = $row["repositorium_veroeffentlichungspflicht"];
        ?>

        <!-- Ausgabe der Daten in einer Tabelle -->
        <table id="infoAusgabe">
            <br>
            <thead>
                <tr>
                    <td class ="bezeichnung" colspan="2"><br><?php echo $infoName ?><br><br></td>
                </tr>
            </thead>
            <tbody>
                <tr class = "infoTr">
                    <td class = "infoTd">Inhalt</td>
                    <td class = "infoTd"><?php echo $infoBeschreibung ?></td>
                </tr>
                <tr class = "infoTr" class = "infoTr">
                    <td class = "infoTd">URL</td>
                    <td class = "infoTd"><?php echo "<a href='$infoUrl' class='plainLink'>$infoUrl</a>"; ?></td>
                </tr>
                <tr class = "infoTr">
                    <td class = "infoTd">Embargo-Periode für Veröffenlichungen</td>
                    <td class = "infoTd"><?php echo booleanUmwandlung($infoEmbargo) ?></td>
                </tr>
                <tr class = "infoTr">
                    <td class = "infoTd">Dateninput möglich</td>
                    <td class = "infoTd"><?php echo booleanUmwandlung($infoInput) ?></td>
                </tr>
                <tr class = "infoTr">
                    <td class = "infoTd">Besondere Nutzungsregeln</td>
                    <td class = "infoTd"><?php echo leerUmwandlung($infoRules) ?></td>
                </tr>
                <tr class = "infoTr">
                    <td class = "infoTd">Repositorium in re3data gelistet</td>
                    <td class = "infoTd"><?php echo booleanUmwandlung($infoRedata) ?></td>
                </tr>
                <tr class = "infoTr">
                    <td class = "infoTd">re3data-Link</td>
                    <td class = "infoTd"><?php echo "<a href='$infoRelink' class='plainLink'>$infoRelink</a>"; ?></td>
                </tr>
                <tr class = "infoTr">
                    <td class = "infoTd">Kosten</td>
                    <td class = "infoTd"><?php echo  tripUmwandlung($infoKosten)?></td>
                </tr>
                <tr class = "infoTr">
                    <td class = "infoTd">Metasystem multipler Repositorien</td>
                    <td class = "infoTd"><?php echo booleanUmwandlung($infoMeta) ?></td>
                </tr>
                <tr class = "infoTr">
                    <td class = "infoTd">Metadaten-Standard Ontologien</td>
                    <td class = "infoTd"><?php echo leerUmwandlung($infoOntologie) ?></td>
                </tr>
                <tr class = "infoTr">
                    <td class = "infoTd">OpenAccess</td>
                    <td class = "infoTd"><?php echo booleanUmwandlung($infoOpen) ?></td>
                </tr>

            </tbody>
        </table>

        <!-- Zurückbutton -->
        <br><br>
        <a id="back" href="<?php echo getenv("HTTP_REFERER"); ?>"> Zurück </a>
    
    </body>
</html>