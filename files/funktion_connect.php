<!--

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<?php //Verbindung zur Datenbank aufbauen und Fehler abfangen und ggf. anzeigen
    try {
        $pdo = new PDO("mysql:host=url;charset=utf8mb4;dbname=...;port=...;","user","PW");
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "Datenbankverbindung erfolgreich <br>"; // Kann zum Debugging eingeblendet werden
        }
    catch(PDOException $e)
        {
        //echo "Verbindungsfehler: " . $e->getMessage(); // nur für Debug!
        echo "Die Verbindung zur Datenbank konnte nicht hergestellt werden.";
        }
?>