<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<!-- Anzeigen der am Anfang initialisierten Navigationselemente innerhalb einer Tabellenzeile -->
<tr>
    <td colspan = "2">
        <br>
        <div id="links">
            <form method="post" action="" accept-charset="UTF-8" class="navButton">
                <input type = "hidden" name = "s" value = "<?php echo $start; ?>">
                <input type = "hidden" name = "richtung" value = "zu" >
                <input type="submit" name="zurück" value="vorherige Ergebnisse anzeigen"> 
            </form>
        </div>
        <div id="rechts">
            <form method="post" action="" accept-charset="UTF-8" class="navButton">
                <input type = "hidden" name = "s" value = "<?php echo $start; ?>">
                <input type = "hidden" name = "richtung" value = "vor">
                <input type="submit" name="weiter" value="weitere Ergebnisse anzeigen">
            </form>
        </div>
        <br>
    </td>
</tr>
