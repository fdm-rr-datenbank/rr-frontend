<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<!-- Auswahl der Fachbereiche wird getroffen und an Variable "selector1" übergeben -->
<h3> Durch die Auswahl von Fachbereichen können sie ihre Suche anpassen</h3>
<div class="select-wrapper">
    <form action="#" method="post">
        <select name="fachbereich">
            <option value="">Fachbereiche Auswählen &#9660</option>
            <option value="AND fachbereich.fachbereich_informatik = 1 ">Informatik</option>
            <option value="AND fachbereich.fachbereich_mathematik = 1 ">Mathematik</option>
            <option value="AND fachbereich.fachbereich_maschinenbau = 1 ">Maschinenbau</option>
            <option value="AND fachbereich.fachbereich_landwirtumwelt = 1 ">Landwirtschaft und Umwelt</option>
            <option value="AND fachbereich.fachbereich_chemie = 1 ">Chemie</option>
            <option value="AND fachbereich.fachbereich_geoinformation = 1 ">Geoinformation</option>
            <option value="AND fachbereich.fachbereich_bwl = 1 ">Wirtschaftswissenschaften</option>
            <option value="AND fachbereich.fachbereich_bauingeneurwesen = 1 ">Bauingenieurwesen</option>
            <option value="AND fachbereich.fachbereich_design = 1 ">Gestaltung</option>
            <option value="AND fachbereich.fachbereich_elektrotechnik = 1 ">Elektrotechnik</option>
        </select>
        <br><br>
        <input type="submit" name="submit" id="sucheButton" value="Suche anpassen"/>
    </form>
</div>

<?php
    $selector1="";
    if(isset($_POST['submit'])){
        $selector1 = $_POST['fachbereich'];             
    }   
?>