<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<!-- vertikale Navigationsleiste mit Links-->
<nav id="navigation">
    <a class="auf" href=" ">HOME</a>
    <a class="auf" href="index.php">DATENBANKSUCHE</a>
    <a class="auf" href="einstieg.php">FAQ</a>
    <a class="auf" href="kontakt.php">KONTAKT</a>
    <a class="auf" href="disclaimer.php">DISCLAIMER</a>
    <a class="auf" href=" ">IMPRESSUM</a>
</nav>
