<!DOCTYPE html>

<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<html lang="de">
    <head>
        <meta charset="utf-8" />
        <title>FAQ</title>
        <link rel="stylesheet" href="style.css">
        <style>
            body { 
                padding: 5%; 
                margin: 13%;
                margin-top: 3%;
                padding-top: 3%;
            }
        </style>
    </head>

    <body>
        <header id="header">
            <img src="logo.png" align="left"  alt="Logo">
            Repositorien Recommender
            <div id="groß">FAQ</div>
        </header>

        <!-- vertikale Navigationsleiste mit Links-->
        <?php
            include('funktion_navigation.php');
        ?>
        <br>
        <h2> Beschreibung </h2>
        <p class="faqtext">Das Repositorien-Recommender-Tool erlaubt Zugriff auf über einhundert Forschungsdatenbanken und Repositorien, wobei die Anzeige der Ergebnisse anhand der gewünschten Fachbereiche und Nutzungsarten angepasst werden kann.</p>

        <h3> Filteroptionen </h3>
        <p> Das Tool gestattet eine Auswahl der folgenden Nutzungsarten:<br>
            <ul style="list-style-type:none">
                <li><div class="listenelement">Archivieren</div> Auswahl aller Plattformen, welche zum Austausch von Daten konzipiert sind. </li>   
                <li><div class="listenelement">Austausch</div> Auswahl aller Repositorien, welche eine langfristige Speicherung von Daten gestatten. </li>   
                <li><div class="listenelement">Nachnutzen</div> Auswahl aller Repositorien, welche der Nutzung bereits existierender Datenbestände dienen. </li>
                <li><div class="listenelement">Veröffentlichen</div> Auswahl aller Repositorien, welche die Veröffenlichung eigener Datenbestände ermöglichen. </li>
                <li><div class="listenelement">Sonstiges</div> Auswahl sämtlicher Repositorien, ohne Filterung nach einer spezifischen Nutzungsart.</li>
            </ul>
        </p>

        <h3>Bedienung</h3>
        <p class="faqtext"> Nach Auswahl der gewünschten Nutzungsart kann eine weitere Anpassung der Suchergebnisse durch Auswahl eines von insgesamt zehn Fachbereichen durchgeführt werden. Durch Auswahl des Buttons "weitere Informationen" können weitere Informationen zu dem Repositorium abgerufen werden, mit dem Button "Recherche Starten" wird das korrespondierende Repositorium geöffnet.</p>

    </body>
</html>