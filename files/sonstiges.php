<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8" />
        <title>DB-Info</title>
        <link rel="stylesheet" href="style.css">
        <style>
            body { 
                padding: 5%; 
                margin: 13%;
                margin-top: 3%;
                padding-top: 3%;
            }
        </style>
    </head>

    <body>
        <!-- Initialisierung der Navigationsbuttons -->
        <?php
          /*  include('funktion_navinit.php');*/
        ?>
        
        <!-- Verbindung zur Datenbank aufbauen und Fehler abfangen und anzeigen -->
        <?php
            include('funktion_connect.php');
        ?>
    
        <!-- Header einbinden-->
        <header id="header">
        <img src="logo.png" align="left"  alt="Logo">
            Forschungsdatenmanagement
            <div id="groß">Repositorien Recommender</div>
        </header>

        <!-- vertikale Navigationsleiste mit Links-->
        <?php
            include('funktion_navigation.php');
        ?>
        
        <!-- Dropdownmenü zur Auswahl der Nutzungsart-->
        <br>
        <h3> Bitte wählen sie aus, in welcher Form sie mit mit Daten interagieren möchten</h3>
        <div class="dropdown">
            <button class="dropbtn">Sonstiges</button>
            <div class="dropdown-content">
                <a href="austausch.php">Austausch</a>
                <a href="nachnutzen.php">Nachnutzen</a>
                <a href="archivieren.php">Archivieren</a>
                <a href="veroeffentlichen.php">Veröffentlichen</a>
            </div>
        </div> 
        <br>
        
        <?php
            /*Auswahl der Fachbereiche treffen und an Variable "selector1" übergeben*/
            include('funktion_fbauswahl.php');

            /*Anzeigen des gegenwärtig Ausgewählten Fachbereiches über den Suchergebnissen*/
            include('funktion_fbfilter.php'); 
        ?>
       
       <!-- Tabelle zur Ausgabe der Daten --> 
        <br>
        <table id="ausgabe">
            <thead>
                <tr>
                    <td 
                        id="auswahl" colspan ="2"> 
                        <?php echo $filter; ?>
                    </td>
                </tr>
                <tr>
                    <td class ="bezeichnung">Bezeichnung</td>
                    <td class ="bezeichnung">Inhalt</td>
                    <td class ="bezeichnung"></td>
                </tr>
            </thead>
            <tbody>

            <!--Einlesen der ausgewählten Datensätze aus der Datenbank-->
            <?php            
                $sql = "SELECT 
                    repositorium.repositorium_name, 
                    repositorium.repositorium_url, 
                    repositorium.repositorium_beschreibung, 
                    repositorium.repositorium_id, 
                    repositorium.repositorium_embargo,
                    repositorium.repositorium_input,
                    repositorium.repositorium_inputrules,
                    repositorium.repositorium_inredata,
                    repositorium.repositorium_kosten,
                    repositorium.repositorium_multiplemeta,
                    repositorium.repositorium_ontologie,
                    repositorium.repositorium_openaccess,
                    repositorium.repositorium_preservation,
                    repositorium.repositorium_relevanz,
                    repositorium.repositorium_relink,
                    repositorium.repositorium_reuse,
                    repositorium.repositorium_sharing,
                    repositorium.repositorium_veroeffentlichungspflicht,
                    fachbereich.repositorium_id, 
                    fachbereich.fachbereich_id,
                    fachbereich.fachbereich_bauingeneurwesen,
                    fachbereich.fachbereich_bwl,
                    fachbereich.fachbereich_chemie,
                    fachbereich.fachbereich_design,
                    fachbereich.fachbereich_elektrotechnik,
                    fachbereich.fachbereich_geoinformation,
                    fachbereich.fachbereich_id,
                    fachbereich.fachbereich_informatik,
                    fachbereich.fachbereich_landwirtumwelt,
                    fachbereich.fachbereich_maschinenbau,
                    fachbereich.fachbereich_mathematik
                    FROM repositorium INNER JOIN fachbereich ON fachbereich.repositorium_id=repositorium.repositorium_id ".$selector1."  GROUP BY repositorium.repositorium_name ORDER BY repositorium.repositorium_relevanz DESC";
                /*Vor entfernen der NavButtons: FROM repositorium INNER JOIN fachbereich ON fachbereich.repositorium_id=repositorium.repositorium_id ".$selector1." AND repositorium.repositorium_reuse = 1 GROUP BY repositorium.repositorium_name ORDER BY repositorium.repositorium_relevanz DESC Limit 5 Offset $start"; */
                /* Ausgabe der ausgewählten Datensätze in Tabellenzeilen */
                foreach ($pdo->query($sql) as $row) :
            ?> 
            <tr id="datTr">
                <td><?=$row['repositorium_name']?></td>
                <td><?=$row['repositorium_beschreibung']?></td>
                <td class="bfield"><a class="linkbutton" href="info.php?id=<?=$row['repositorium_id']?>">Weitere<br>Informationen</a></td>
                <td class="bfield"><a class="linkbutton" href="<?=$row['repositorium_url']?>">Recherche<br>starten</a></td>
            </tr>
            <?php 
                endforeach; 
                    
                /*Anzeigen der am Anfang initialisierten Navigationselemente*/
                /*include('funktion_navbutton.php'); */                 
                ?>

                </tbody> 
            </table>
               
        </body>
        </html>