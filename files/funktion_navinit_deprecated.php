<!-- 

Copyright 2019 HTW Dresden

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

<!-- Initialisierung der Navigationselemente, muss (!) direkt am Anfang des body stehen -->
<?php
    $start = 1; 
    if (isset ($_POST["s"])) {$start = $_POST["s"];}
    if (isset ($_POST["richtung"])) {    
        if ($_POST["richtung"] == "zu") {
            $start = $start - 5; 
        }
        if ($_POST["richtung"] == "vor") {
            $start = $start + 5;                 
        }
    }
    if ($start <= 0) {$start = 0;
    }
    if ($start > 10) {$start = 10;
    }
?>