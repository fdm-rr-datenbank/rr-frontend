Die Datenbank des Repositorien Recommender der HTW Dresden ist lizensiert unter der <a href="http://opendatacommons.org/licenses/odbl/1.0/">Open Database License ODbL v1.0</a> . 

Die individuellen Inhalte der Datenbank des Repositorien Recommender der HTW Dresden sind lizensiert unter der <a href="http://opendatacommons.org/licenses/dbcl/1.0/">Database Contents License DbCL v1.0</a>. 